#!/usr/bin/env bash

java_home=/home/misha/.jdks/corretto-1.8.0_402
export JAVA_HOME=${java_home}
java=${java_home}/bin/java

cd ../build/libs || exit 1

${java} -jar test.jar
