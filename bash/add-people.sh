#!/usr/bin/env bash

curl -X POST http://localhost:8989/people/add \
  -H 'Content-Type: application/json' \
  -d '[{"name":"Margo", "sex": "0"}, {"name":"Paul", "sex": "1"}]'
