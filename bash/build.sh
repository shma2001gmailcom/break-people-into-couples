#!/usr/bin/env bash

java_home=/home/misha/.jdks/corretto-1.8.0_402
export JAVA_HOME=${java_home}

cd ..
bash gradlew clean build -x test
