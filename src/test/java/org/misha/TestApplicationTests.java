package org.misha;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.misha.entity.People;
import org.misha.service.PeopleService;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static java.lang.Thread.currentThread;
import static java.util.Comparator.comparing;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class TestApplicationTests {
    private static final Logger log = LoggerFactory.getLogger(TestApplicationTests.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private final PeopleService mock;

    TestApplicationTests() {
        mock = Mockito.mock(PeopleService.class);
        when(mock.splitIntoPairs()).thenReturn(mockSplit());
    }

    private static List<People> expected() {
        return readPeople(currentThread().getContextClassLoader().getResourceAsStream("pairs.json"));
    }

    private static List<People> mockSplit() {
        List<People> all = allPeople();
        Comparator<People> comparing = comparing(People::getName);
        List<People> women = all.stream().filter(p -> p.getSex() == 0).sorted(comparing).collect(Collectors.toList());
        List<People> men = all.stream().filter(p -> p.getSex() == 1).sorted(comparing).collect(Collectors.toList());
        List<People> pairs = new ArrayList<>();
        int w = 0, m = 0;
        while (w < women.size() || m < men.size()) {
            if (m < men.size()) {
                pairs.add(men.get(m++));
            }
            if (w < women.size()) {
                pairs.add(women.get(w++));
            }
        }
        return pairs;
    }

    private static List<People> allPeople() {
        return new ArrayList<>(readPeople(currentThread().getContextClassLoader().getResourceAsStream("all.json")));
    }

    private static List<People> readPeople(InputStream resourceAsStream) {
        try {
            return mapper.readValue(resourceAsStream, new TypeReference<List<People>>() {
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void checkQuery() {
        log.info(() -> "start checkQuery");
        List<People> expected = expected();
        List<People> actual = mock.splitIntoPairs();
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); ++i) {
            People e = expected.get(i);
            People a = actual.get(i);
            log.info(() -> format("pair: %s = %s", a.getName(), e.getName()));
            assertEquals(e, a);
        }
    }
}
