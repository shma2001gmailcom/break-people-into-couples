package org.misha.service;

import org.misha.entity.People;
import org.misha.repo.PeopleRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
public class PeopleService {
    private static final Logger log = LoggerFactory.getLogger(PeopleService.class);
    private final PeopleRepo peopleRepo;
    private final Cache cache;

    public PeopleService(PeopleRepo peopleRepo, Cache cache) {
        this.peopleRepo = peopleRepo;
        this.cache = cache;
    }

    @Transactional
    public List<People> splitIntoPairs() {
        return peopleRepo.pairs();
    }

    @Transactional
    public List<People> getAll() {
        List<People> result = new ArrayList<>();
        peopleRepo.findAll().forEach(p -> {
            if (cache.get(p.getId()) == null) {
                cache.put(p.getId(), p);
                log.info("new {}", p);
                result.add(p);
            } else {
                log.info("from cache {}", p);
                Cache.ValueWrapper valueWrapper = cache.get(p.getId());
                if (valueWrapper != null) {
                    result.add((People) valueWrapper.get());
                }
            }
        });
        return result;
    }

    @Transactional
    public void add(String name, int sex) {
        People people = new People();
        people.setSex(sex);
        people.setName(name);
        People saved = peopleRepo.save(people);
        Integer id = saved.getId();
        if (cache.get(id) == null) {
            cache.put(id, saved);
            log.info("cache put {}", saved);
        }
    }

    @Transactional
    public void saveAll(List<People> people) {
        peopleRepo.save(people);
    }
}
