package org.misha.repo;

import javax.transaction.Transactional;
import org.misha.entity.People;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeopleRepo extends JpaRepository<People, Integer> {
    Logger log = LoggerFactory.getLogger(PeopleRepo.class);

    @Query(value = "SELECT id, name, sex FROM((SELECT id, name, sex, 2 * row_number() OVER ( PARTITION BY sex ) ord " +
            "FROM (SELECT * FROM people WHERE sex = 0 ORDER BY name) fem) " +
            "UNION SELECT id, name, sex, -1 + 2 * row_number() OVER (PARTITION BY sex ) ord " +
            "FROM (SELECT * FROM people WHERE sex = 1 ORDER BY name) mal) pairs ORDER BY ord;", nativeQuery = true)
    List<People> breakIntoPairs();

    @Transactional
    default List<People> pairs() {
        try {
            List<People> pairs = breakIntoPairs();
            log.info("fetched pairs: {}", pairs);
            return pairs;
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
