package org.misha.http;

import org.misha.entity.People;
import org.misha.service.PeopleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SuppressWarnings("unused")
@RestController
public class PeopleController {
    private static final Logger log = LoggerFactory.getLogger(PeopleController.class);
    private final PeopleService service;

    public PeopleController(PeopleService service) {
        this.service = service;
    }

    @GetMapping(path = "/people/pairs", produces = "application/json")
    public ResponseEntity<?> pairs() {
        try {
            List<People> people = service.splitIntoPairs();
            log.debug("PEOPLE fetched: \n\n {}", people);
            return new ResponseEntity<>(people, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(path = "/people/all", produces = "application/json")
    public ResponseEntity<?> all() {
        try {
            List<People> people = service.getAll();
            log.debug("PEOPLE fetched: \n\n {}", people);
            return new ResponseEntity<>(people, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/people/add", produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> add(@RequestBody List<People> people) {
        log.debug("PEOPLE to save: \n\n {}", people);
        try {
            service.saveAll(people);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(path = "/people/add-single", produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> add(@RequestBody People people) {
        log.debug("PEOPLE to save: \n\n {}", people);
        try {
            service.add(people.getName(), people.getSex());
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
