package org.misha.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SuppressWarnings("unused")
@EnableAspectJAutoProxy
@EntityScan("org.misha.entity.**")
@EnableJpaRepositories("org.misha.repo")
@EnableTransactionManagement
@Configuration
public class PeopleJdbcConfig {

    @Primary
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSourceProperties dataSourceProperties() {
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        System.err.println("\n\n\n\n           data source properties:\n" + dataSourceProperties.getDriverClassName());
        return dataSourceProperties;
    }

    @Bean
    @ConfigurationProperties("spring.datasource")
    public HikariDataSource hikariDataSource() {
        return (HikariDataSource) dataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class)
                .build();
    }
}
