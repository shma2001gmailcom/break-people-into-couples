package org.misha.config;

import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.core.config.DefaultConfiguration;
import org.ehcache.impl.config.persistence.DefaultPersistenceConfiguration;
import org.ehcache.jsr107.EhcacheCachingProvider;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.Caching;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("unused")
@Configuration
@EnableCaching
public class PeopleCacheConfig {

    @Bean
    public org.springframework.cache.CacheManager cacheManager() {
        CacheConfiguration<Object, Object> config = CacheConfigurationBuilder.newCacheConfigurationBuilder(
                Object.class, Object.class,
                ResourcePoolsBuilder.newResourcePoolsBuilder()
                        .heap(500, EntryUnit.ENTRIES)
                        .offheap(1, MemoryUnit.GB)
                        .disk(3, MemoryUnit.GB, true)
        ).build();
        Map<String, CacheConfiguration<?, ?>> caches = new HashMap<>();
        caches.put("people", config);
        EhcacheCachingProvider ehcacheProvider = (EhcacheCachingProvider) Caching.getCachingProvider();
        DefaultConfiguration configuration = new DefaultConfiguration(
                caches,
                ehcacheProvider.getDefaultClassLoader(),
                new DefaultPersistenceConfiguration(new File("./ehcache")));
        return new JCacheCacheManager(
                ehcacheProvider.getCacheManager(
                        ehcacheProvider.getDefaultURI(), configuration));
    }

    @Bean
    Cache cache() {
        return cacheManager().getCache("people");
    }
}
