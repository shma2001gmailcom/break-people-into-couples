DROP TABLE people;

CREATE TABLE people
(
  id   SERIAL PRIMARY KEY,
  name TEXT,
  sex  INTEGER
);
INSERT INTO public.people (name, sex) VALUES ('Jane', 0);
INSERT INTO public.people (name, sex) VALUES ('Tim', 1);
INSERT INTO public.people (name, sex) VALUES ('Al', 1);
INSERT INTO public.people (name, sex) VALUES ('Beth', 0);
INSERT INTO public.people (name, sex) VALUES ('Ann', 0);
INSERT INTO public.people (name, sex) VALUES ('Pit', 1);
INSERT INTO public.people (name, sex) VALUES ('Bill', 1);
INSERT INTO public.people (name, sex) VALUES ('Liz', 0);
INSERT INTO public.people (name, sex) VALUES ('Sam', 1);
INSERT INTO public.people (name, sex) VALUES ('Peggy', 0);
INSERT INTO public.people (name, sex) VALUES ('Mike', 1);
INSERT INTO public.people (name, sex) VALUES ('Ruth', 0);