SELECT
  id,
  name,
  sex
FROM ((SELECT
         id,
         name,
         sex,
         2 * row_number()
         OVER (
           PARTITION BY sex ) ord
       FROM (SELECT *
             FROM people
             WHERE sex = 0
             ORDER BY name) fem)
      UNION
      SELECT
        id,
        name,
        sex,
        -1 + 2 * row_number()
        OVER (
          PARTITION BY sex ) ord
      FROM (SELECT *
            FROM people
            WHERE sex = 1
            ORDER BY name) mal) pairs
ORDER BY ord;

